
@Grapes([
        @Grab('org.apache.poi:poi:3.10.1'),
        @Grab('org.apache.poi:poi-ooxml:3.10.1')])
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.text.Normalizer
import static org.apache.poi.ss.usermodel.Cell.*
import org.apache.poi.ss.usermodel.Row
import java.nio.file.Paths

def path = "/home/eric/Documents/development/ufpr/Solid Eric/ItemPedido.xlsx"
def relation = "kd-itempedido"
def columns = ["tipopedido", "aprovado", "fasepedido", "statusinternopedido", "canal", "origem", "mercado", "cluster", "statusitem","statusproduto", "polo"]
def arff = "ItemPedido.arff"
new XslxToArffParser(path, relation, columns, arff);

class Data{
    def rows = new ArrayList<List>();

    @Override
    String toString() {
        def s = ""
        for (r in rows){
            for(d in r){

                s+=d
                if(r.indexOf(d) < (r.size()-1))
                    s+=","
            }
            s+="\n"
        }
        return s
    }
}



class Atributo {
    def descricao;
    def possibilidades = new HashSet<Object>();
    def index;

    @Override
    String toString() {

        def builder = new StringBuilder()
        builder.append("@attribute ").append(descricao)
        builder.append(" {")
        for(def i = 0; i<possibilidades.size(); i++){
            builder.append(possibilidades[i])
            if((i+1) != possibilidades.size())
                builder.append(",")
        }
        builder.append("}").append("\n")
        return builder.toString();
    }
}

class XslxToArffParser {
    def attributes =[:];
    def data = new Data();
    def sheet = null;

    XslxToArffParser(path, relation, columns, arffPath){
        load(path)
        getAttributes(columns)
        collectData()
        saveArff(relation, arffPath)
    }

    def String parse(String s){
        s = Normalizer.normalize(s, Normalizer.Form.NFD)
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
        s = s.split(/[^\w]/).collect { it.toLowerCase().capitalize() }.join("")
        s = s.replaceAll(" ", "")
        s = s.replaceAll("[^A-Za-z0-9]", "")
        s = s.isEmpty() ? "Nada" : s
        return s
    }

    def load(path) {
        Paths.get(path).withInputStream { input ->
            def workbook = new XSSFWorkbook(input)
            sheet = workbook.getSheetAt(0)
        }
    }

    def getAttributes(columns){
        for (cell in sheet.getRow(0).cellIterator()) {
            def index = cell.columnIndex
            def description = parse(cell.stringCellValue).toLowerCase()
            if(columns.contains(description)){
                attributes << [(index):new Atributo(descricao: description, index: index)]
            }
        }
    }

    def collectData(){
        def headerFlag = true
        for (row in sheet.rowIterator()) {
            if (headerFlag) {
                headerFlag = false
                continue
            }
            def r = []

            for (int index = 0; index <= row.lastCellNum; index++) {
                def attr = attributes[index]
                def cell = row.getCell(index, Row.RETURN_BLANK_AS_NULL);

                if(attr != null){
                    def value = ""

                    if(cell == null) value = parse(value)
                    else value = cell.cellType == CELL_TYPE_STRING ? parse(cell.stringCellValue) : cell.numericCellValue

                    attr.possibilidades.add(value)
                    r << value
                }

            }
            data.rows.add(r)
        }
    }

    def saveArff(relation, path){
        Paths.get(path).withWriter { writer ->

            writer.write "@relation " + relation
            writer.write "\n"
            for(a in attributes.values())
                writer.write a.toString()

            writer.write "@data"
            writer.write "\n"

            writer.write data.toString()
        }
    }
}